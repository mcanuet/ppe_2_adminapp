window.onload = function(){
  init();
  M.AutoInit();
};

let allRights = new Object();
let groups = new Array();

function init() {
  getAllRights();
}

function getAllRights() {
  $.ajax({
    url: config.api_serv+'right',
    headers: {'token': sessionStorage.getItem('token')},
    method: "GET",
    statusCode:{
      200: function (e) {
        serializeRights(e);
        getGroups();
      }
    }
  });
}

function serializeRights(data) {
  for (let i in data){
    if ( allRights[data[i].method]){
      allRights[data[i].method].push({id: i, table: data[i].table});
    } else {
      allRights[data[i].method] = [{id: i, table: data[i].table}];
    }
  }
}

function getGroups() {
  $.ajax({
    url: config.api_serv+'group',
    headers: {'token': sessionStorage.getItem('token')},
    method: "GET",
    statusCode:{
      200: function (e) {
        groups = e;
        createCard();
      }
    }
  });
}

function createCard() {
  for (let i of groups){
    $("#cardContainer").append(
      "<div class='col s4'>" +
      "<div class='card'>" +
      "<div class='card-content'>" +
      "<span class='card-title'>" +
      "<span class='col s10'> id: " + i.id + " - " + i.name + " </span>" +
      "<span class='class col s2'> <i class='material-icons delete' group='" + i.id + "'>delete</i> </span>" +
      "</span>" +
      "<div class='row' id='chipsContainer_"+i.id+"'>" +
      "</div>" +
      "</div>" +
      "<div class='card-action'>" +
      "<div class='row'>" +
      "<div class='input-field col s8'>" +
      "<select multiple id='select_"+i.id+"'>" +
      "</select>" +
      "<label>Droits</label>" +
      "</div>" +
      "<div class='col s3'>" +
      "<a class='btn-floating btn-large waves-effect waves-light green darken-1 addRight'><i id='addBtn_"+i.id+"' class='material-icons'>add</i></a>" +
      "</div>" +
      "</div>" +
      "</div>" +
      "</div>" +
      "</div>"
    );
    createChips(i.id, i);
    creatOptions(i.id);
  }
  M.AutoInit();
}

function createChips(id, group) {
  if (group.rights) {
    for (let i of group.rights){
      if (i.method !== "OPTIONS") {
        $("#chipsContainer_"+id).append(
          "<div class='chip'>" + i.method + " / " + i.table + "<i class='close material-icons' right='" + i.id + "' group='"+ group.id +"'>close</i> </div>"
        )
      }
    }
  }
}

function creatOptions(id) {
  let html = "";
  for (let i in allRights){
      if (i !== "OPTIONS") {
        html += "<optgroup label='"+ i +"'>";
        for (let j of allRights[i]){
          html += "<option value='"+j.id+"' right='" + j.id + "'>"+ j.table +"</option>";
        }
      }
  }
  html += "</optgroup>";
  $("#select_"+id).append(html);
  createEvents();
}

function createEvents() {
  $(".addRight").click(function (e) {
    e.stopPropagation();
    e.stopImmediatePropagation();
    let rightsToSend = new Array();
    const id = e.target.id.replace('addBtn_', '');
    const rightsSelected = $("#select_" + id)[0].selectedOptions;
    const rightsCurrent = $("#chipsContainer_" + id)[0].childNodes;
    for (let right of rightsSelected) { rightsToSend.push(parseInt(right.value) + 1); }
    for (let right of rightsCurrent) { rightsToSend.push(parseInt(right.childNodes[1].attributes.right.value)) }
    sendRequest(id, rightsToSend, true);
  });

  $(".close").click(function (e) {
    const id = e.target.attributes.group.value;
    const rightToDelete = parseInt(e.target.attributes.right.value);
    const rights = e.target.parentNode.parentNode.childNodes;
    let rightsToSend = new Array();
    for (let right of rights) { rightsToSend.push(parseInt(right.childNodes[1].attributes.right.value)) }
    rightsToSend.splice(rightsToSend.indexOf(rightToDelete),1);
    sendRequest(id, rightsToSend);
  });

  $(".delete").click(function (e) {
    e.stopPropagation();
    e.stopImmediatePropagation();
    const id = e.target.attributes.group.value;
    $.ajax({
      url: config.api_serv+"group?id=" + id,
      headers: {'token': sessionStorage.getItem('token')},
      method: "DELETE",
      statusCode: {
        204: function () {
          M.toast({html: 'groupe supprimé'});
          $("#cardContainer")[0].innerHTML = '';
          init();
        }
      }
    });
  });
}

function sendRequest(id, rights, reload=false) {
  const data = JSON.stringify({id: id, rights: rights});
  $.ajax({
    url: config.api_serv+"group",
    headers: {'token': sessionStorage.getItem('token')},
    method: "PUT",
    data: data,
    statusCode: {
      204: function () {
        M.toast({html: 'droit modifé'});
        if (reload){
          $("#cardContainer")[0].innerHTML = '';
          init();
        }
      }
    }
  });
}

$("#addBtn").click(function () {
  const groupName = $("#addGroup")[0].value;
  if (groupName !== ''){
    $.ajax({
      url: config.api_serv+"group",
      headers: {'token': sessionStorage.getItem('token')},
      method: "POST",
      data: JSON.stringify({name: groupName, rights: []}),
      statusCode: {
        201: function () {
          M.toast({html: 'group ajouté !'});
          $("#cardContainer")[0].innerHTML = '';
          init();
        }
      }
    });
  }
});

