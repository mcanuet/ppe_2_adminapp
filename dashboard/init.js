init();
function init() {
  const val = ['car', 'model'];
  setReservation();
  for (let i of val) {
    metrics(i);
  }
}

function metrics(entity) {
  $.ajax({
    url: config.api_serv+entity+"/metrics",
    headers: {'token': sessionStorage.getItem('token')},
    method: "GET",
    statusCode:{
      200: function (e) {
        if (entity === 'car') setCarBoard(e);
        if (entity === 'model') setModelBoard(e);
      }
    }
  });
}

function setCarBoard(data) {
  $("#car").append("<ul class='tabs' id='carTabs'></ul>");
  let cpt = 0;
  for (let i in data) {
    if (i !== "total"){
      $("#carTabs").append("<li class='tab'><a href='#car"+cpt+"'>"+i+"</a></li>");
      $("#car").append("<div class='col s12' id='car"+cpt+"'>" +
          "<table class='striped'>" +
          "<thead>" +
          "<tr>" +
          "<td>model</td>" +
          "<td>nombre de voitures</td>"+
          "</tr>" +
          "</thead>" +
          "<tbody id='carTbody"+cpt+"'></tbody>" +
          "</table>" +
          "</div>");
    }
    for (let j in data[i]) {
      if (j !== "total"){
        $("#carTbody"+cpt).append("<tr>" +
            "<td>"+j+"</td>"+
            "<td>"+data[i][j]+"</td>"+
            "</tr>");
      }
    }
    cpt ++;
  }
  M.AutoInit();
}

function setModelBoard(data) {
  $("#model").append("<table class='striped'>" +
      "<thead>" +
        "<tr>" +
          "<td>marque</td>" +
          "<td>models</td>" +
        "</tr>" +
      "</thead>" +
      "<tbody id='modelTbody'></tbody>" +
    "</table>");
  let cpt = 0;
  for (let i in data) {
    let models = "";
    for (j of data[i]['model']){
      models += j+" || "
    }
    models = models.substring(0, models.length-4);
    $("#modelTbody").append("<tr>" +
        "<td>"+i+"</td>" +
        "<td>"+models+"</td>" +
      "</tr>");
    cpt ++;
  }
}

function setReservation() {
  let date = new Date().toISOString().substring(0,10);
  $.ajax({
    url: config.api_serv+"reservation?endgt="+date,
    headers: {'token': sessionStorage.getItem('token')},
    method: "GET",
    statusCode:{
      200: function (e) {
        createReservationText(e)
      }
    }
  });
}

function createReservationText(data) {
  $("#reservation").append("<table class='striped'>" +
    "<thead>" +
      "<tr>" +
        "<td>email</td>" +
        "<td>voiture</td>" +
        "<td>start</td>" +
        "<td>end</td>" +
      "</tr>" +
    "</thead>" +
    "<tbody id='reservationTbody'></tbody>" +
    "</table>");
  let cpt = 0;
  while (cpt < 20 && data[cpt].user) {
    $("#reservationTbody").append("<tr>" +
      "<td>"+data[cpt].user.email+"</td>" +
      "<td>"+data[cpt].car.plate_number+"</td>" +
      "<td>"+data[cpt].start+"</td>" +
      "<td>"+data[cpt].end+"</td>" +
      "</tr>");
    cpt ++;
  }
}