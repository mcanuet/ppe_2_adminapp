$("#navbar").load('../common_html/navbar.html', function () {
  navEvent();
});

function navEvent(){
  $("#dashboard_nav").click(function () {
    window.location = config.front_url+'dashboard'
  });
  $("#car_nav").click(function () {
    window.location = config.front_url+'car'
  });
  $("#model_nav").click(function () {
    window.location = config.front_url+'model'
  });
  $("#user_nav").click(function () {
    window.location = config.front_url+'user'
  });
  $("#reservation_nav").click(function () {
    window.location = config.front_url+'reservation'
  });
  $("#group_rights_nav").click(function () {
    window.location = config.front_url+'groupRights'
  });
}