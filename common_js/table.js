$("#mainContainer").load('../common_html/manage.html', function () {
  init('',event);
  M.AutoInit();
});

function init(filter = '', callback) {
  const entity = window.location.pathname.split('/')[2];
  $.ajax({
    url: config.api_serv+entity+'?'+filter,
    headers: {'token': sessionStorage.getItem('token')},
    method: "GET",
    statusCode:{
      200: function (e) {
        $("#thead")[0].innerHTML= "";
        $("#tbody")[0].innerHTML= "";
        $("#modalContainer")[0].innerHTML = "";
        if (entity === 'car') carTable(e, callback);
        if (entity === 'model') modelTable(e, callback);
        if (entity === 'user') userTable(e, callback);
        if (entity === 'reservation') reservationTable(e, callback);
      }
    }
  });
}

function carTable(data, callback) {
  $("#thead").append(
    "<th>id</th>" +
    "<th>plate_number</th>" +
    "<th>kilometers_count</th>" +
    "<th>last_control</th>" +
    "<th>ref_insurance</th>" +
    "<th>status</th>" +
    "<th>model_id</th>" +
    "<th>disponibility</th>" +
    "<th>modifier</th>" +
    "<th>supprimer</th>"
  );
  for (let i of data) {
    $("#tbody").append(
      "<tr id='car_"+i.id+"'>" +
        "<td>"+i.id+"</td>" +
        "<td><input type='text' name='plate_number' id='plate_number_"+i.id+"' value='"+i.plate_number+"'></td>" +
        "<td><input type='text' name='kilometers_count' id='kilometers_count_"+i.id+"' value='"+i.kilometers_count+"'></td>" +
        "<td><input type='text' name='last_control' id='last_control_"+i.id+"' value='"+i.last_control+"'></td>" +
        "<td><input type='text' name='ref_insurance' id='ref_insurance_"+i.id+"' value='"+i.ref_insurance+"'></td>" +
        "<td><input type='text' name='status' id='status_"+i.id+"' value='"+i.status+"'></td>" +
        "<td><input type='text' name='model_id' id='model_id_"+i.id+"' value='"+i.model_id+"'></td>" +
        "<td><input type='text' name='disponibility' id='disponibility_"+i.id+"' value='"+i.disponibility+"'></td>" +
        "<td><a class='btn-floating btn-large waves-effect waves-light blue edit' id='_"+i.id+"'><i class='material-icons'>edit</i></a</td>" +
        "<td><a class='btn-floating btn-large waves-effect waves-light red delete' id='"+i.id+"'><i class='material-icons'>close</i></a</td>" +
      "</tr>"
    );
  }
  $("#modalContainer").append(
    "<form id='addForm'>" +
    "<input type='text' name='plate_number' id='add_plate_number' required value='plate_number'>" +
    "<input type='text' name='kilometers_count' id='add_kilometers_count' required value='kilometers_count'>" +
    "<input type='text' name='last_control' id='add_last_control' required value='last_control'>" +
    "<input type='text' name='ref_insurance' id='add_ref_insurance' required value='ref_insurance'>" +
    "<input type='text' name='status' id='add_status' required value='status'>" +
    "<input type='text' name='model_id' id='add_model_id' required value='model_id'>" +
    "<input type='text' name='disponibility' id='add_disponibility' required value='disponibility'>" +
    "</form>"
  );
  callback('car');
}

function modelTable(data, callback) {
  $("#thead").append(
    "<th>id</th>" +
    "<th>brand</th>" +
    "<th>model</th>" +
    "<th>fuel_type</th>" +
    "<th>image</th>" +
    "<th>places</th>" +
    "<th>engine</th>" +
    "<th>category</th>" +
    "<th>modifier</th>" +
    "<th>supprimer</th>"
  );
  for (let i of data) {
    if (i.id){
      $("#tbody").append(
        "<tr id='model_"+i.id+"'>" +
        "<td>"+i.id+"</td>" +
        "<td><input type='text' name='brand' id='brand_"+i.id+"' value='"+i.brand+"'></td>" +
        "<td><input type='text' name='model' id='"+i.id+"' value='"+i.model+"'></td>" +
        "<td><input type='text' name='fuel_type' id='fuel_type_"+i.id+"' value='"+i.fuel_type+"'></td>" +
        "<td><input type='text' name='image' id='image_"+i.id+"' value='"+i.image+"'></td>" +
        "<td><input type='text' name='places' id='places_"+i.id+"' value='"+i.places+"'></td>" +
        "<td><input type='text' name='engine' id='engine_"+i.id+"' value='"+i.engine+"'></td>" +
        "<td><input type='text' name='category' id='category_"+i.id+"' value='"+i.category+"'></td>" +
        "<td><a class='btn-floating btn-large waves-effect waves-light blue edit' id='_"+i.id+"'><i class='material-icons'>edit</i></a</td>" +
        "<td><a class='btn-floating btn-large waves-effect waves-light red delete' id='"+i.id+"'><i class='material-icons'>close</i></a</td>" +
        "</tr>"
      );
    }
  }
  $("#modalContainer").append(
    "<form id='addForm'>" +
    "<input type='text' name='brand' id='add_brand' required value='brand'>" +
    "<input type='text' name='model' id='add_model' required value='model'>" +
    "<input type='text' name='fuel_type' id='add_fuel_type' required value='fuel_type'>" +
    "<input type='text' name='image' id='add_image' required value='image'>" +
    "<input type='text' name='places' id='add_places' required value='places'>" +
    "<input type='text' name='engine' id='add_engine' required value='engine'>" +
    "<input type='text' name='category' id='add_category' required value='category'>" +
    "</form>"
  );
  callback('model');
}

function userTable(data, callback) {
  $("#thead").append(
    "<th>id</th>" +
    "<th>email</th>" +
    "<th>password</th>" +
    "<th>phone</th>" +
    "<th>zip_code</th>" +
    "<th>town</th>" +
    "<th>street_name</th>" +
    "<th>street_number</th>" +
    "<th>id_group</th>" +
    "<th>modifier</th>" +
    "<th>supprimer</th>"
  );
  for (let i of data) {
    $("#tbody").append(
      "<tr id='user_"+i.id+"'>" +
      "<td>"+i.id+"</td>" +
      "<td><input type='text' name='email' id='email_"+i.id+"' value='"+i.email+"'></td>" +
      "<td><input type='password' name='password' id='password_"+i.id+"' value='"+i.password+"'></td>" +
      "<td><input type='text' name='phone' id='phone_"+i.id+"' value='"+i.phone+"'></td>" +
      "<td><input type='text' name='zip_code' id='zip_code_"+i.id+"' value='"+i.zip_code+"'></td>" +
      "<td><input type='text' name='town' id='town_"+i.id+"' value='"+i.town+"'></td>" +
      "<td><input type='text' name='street_name' id='street_name_"+i.id+"' value='"+i.street_name+"'></td>" +
      "<td><input type='number' name='street_number' id='street_number_"+i.id+"' value='"+i.street_number+"'></td>" +
      "<td><input type='number' name='id_group' id='id_group_"+i.id+"' value='"+i.id_group+"'></td>" +
      "<td><a class='btn-floating btn-large waves-effect waves-light blue edit' id='_"+i.id+"'><i class='material-icons'>edit</i></a</td>" +
      "<td><a class='btn-floating btn-large waves-effect waves-light red delete' id='"+i.id+"'><i class='material-icons'>close</i></a</td>" +
      "</tr>"
    );
  }
  $("#modalContainer").append(
    "<form id='addForm'>" +
    "<input type='email' name='email' id='add_email' required placeholder='email'>" +
    "<input type='password' name='password' id='add_password' required placeholder='password'>" +
    "<input type='text' name='phone' id='add_phone' required placeholder='phone'>" +
    "<input type='text' name='zip_code' id='add_zip_code' required placeholder='zip_code'>" +
    "<input type='text' name='town' id='add_town' required placeholder='town'>" +
    "<input type='text' name='street_name' id='add_street_name' required placeholder='street_name'>" +
    "<input type='number' name='street_number' id='add_street_number' required placeholder='street_number'>" +
    "<input type='number' name='id_group' id='add_id_group' required placeholder='id_group'>" +
    "</form>"
  );
  callback('user');
}

function reservationTable(data, callback) {
  $("#thead").append(
    "<th>id</th>" +
    "<th>car_id</th>" +
    "<th>user_id</th>" +
    "<th>start</th>" +
    "<th>end</th>" +
    "<th>modifier</th>" +
    "<th>supprimer</th>"
  );
  for (let i of data) {
    if (i.id){
      $("#tbody").append(
        "<tr id='reservation_"+i.id+"'>" +
        "<td>"+i.id+"</td>" +
        "<td><input type='number' min='1' name='car_id' id='id_"+i.id+"' value='"+i.car.id+"'></td>" +
        "<td><input type='number' min='1' name='user_id' id='user_id_"+i.id+"' value='"+i.user.id+"'></td>" +
        "<td><input type='text' name='start' id='start_"+i.id+"' value='"+i.start+"'></td>" +
        "<td><input type='text' name='end' id='end_"+i.id+"' value='"+i.end+"'></td>" +
        "<td><a class='btn-floating btn-large waves-effect waves-light blue edit' id='_"+i.id+"'><i class='material-icons'>edit</i></a</td>" +
        "<td><a class='btn-floating btn-large waves-effect waves-light red delete' id='"+i.id+"'><i class='material-icons'>close</i></a</td>" +
        "</tr>"
      );
    }
  }
  $("#modalContainer").append(
    "<form id='addForm'>" +
    "<input type='number' name='car_id' id='add_car_id' required value='car_id'>" +
    "<input type='number' name='user_id' id='add_user_id' required value='user_id'>" +
    "<input type='text' name='start' id='add_start' required value='start'>" +
    "<input type='text' name='end' id='add_end' required value='end'>" +
    "</form>"
  );
  callback('reservation');
}

function event(entity) {
  $("#searchBtn").click(function () {
    init($("#search")[0].value, event);
  });

  $("#addBtn").click(function () {
    $(".modal").modal('open');
  });

  $("#confirmAdd").click(function () {
    const rawdata = $("#addForm").serialize().split('&');
    let data = {};
    for (let i of rawdata) {
      let split = i.split('=');
        data[split[0]] = split[1];
    }
    sendPost(JSON.stringify(data).replace("%40","@"), entity);
  });

  $(".delete").click(function (e) {
    sendDelete(e.target.parentElement.id, entity);
  });

  $(".edit").click(function (e) {
    const id = e.target.parentElement.id.substring(1);
    const target = "#"+entity+"_"+id;
    let data = {id: id};
    for (let i = 1; i < $(target)[0].childNodes.length -2; i++) {
      data[$(target)[0].childNodes[i].children[0].name] = $(target)[0].childNodes[i].children[0].value;
    }
    sendPut(JSON.stringify(data), entity);
  });
}

function sendPost(data, entity) {
  $.ajax({
    url: config.api_serv+entity,
    headers: {'token': sessionStorage.getItem('token')},
    method: "POST",
    data: data,
    statusCode: {
      200: function () {
        M.toast({html: entity+': ajouter'});
        init($("#search")[0].value, event);
      }
    }
  });
}

function sendDelete(id, entity) {
  $.ajax({
    url: config.api_serv+entity+'?id='+id,
    headers: {'token': sessionStorage.getItem('token')},
    method: "DELETE",
    statusCode: {
      200: function () {
        M.toast({html: entity+': supprimer'});
        init($("#search")[0].value, event);
      }
    }
  });
}

function sendPut(data, entity) {
  $.ajax({
    url: config.api_serv+entity,
    headers: {'token': sessionStorage.getItem('token')},
    method: "PUT",
    data: data,
    statusCode: {
      200: function () {
        M.toast({html: entity+': modifé'});
        init($("#search")[0].value, event);
      }
    }
  });
}