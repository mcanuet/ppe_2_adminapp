$("#submit").click(function (e) {
    e.preventDefault();
    const data = {
        "email": $("#email")[0].value,
        "password": $("#pwd")[0].value
    };
    $.ajax({
        url: config.api_serv+"user/login",
        method: "POST",
        crossOrigin: true,
        data: JSON.stringify(data),
        statusCode:{
            200: function (e) {
                setStorage(e);
            },
            404: function () {
                $(".modal").modal('open');
            },
        }
    });
});

function setStorage(data) {
    this.sessionStorage.setItem('token', data.token);
    this.sessionStorage.setItem("self", JSON.stringify(data.self));
    // this.sessionStorage.setItem('id', data.self.id);
    // this.sessionStorage.setItem('email', data.self.email);
    // this.sessionStorage.setItem('phone', data.self.phone);
    // this.sessionStorage.setItem('zip_code', data.self.zip_code);
    // this.sessionStorage.setItem('town', data.self.town);
    // this.sessionStorage.setItem('street_name', data.self.street_name);
    // this.sessionStorage.setItem('street_number', data.self.street_number);
    // this.sessionStorage.setItem('role', data.self.role);
    window.location = config.front_url+"dashboard";
}