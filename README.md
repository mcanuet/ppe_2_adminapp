# ppe_2_adminApp

## Installation

### Pérequis

- serveur web Apache / Nginx
- git
- projet `ppe_2_backend` installé

### Récupération du projet

`git clone https://gitlab.com/mcanuet/ppe_2_adminapp.git`

### Configuration

renommer le fichier `/common_js/config.js.lock` en `/common_js/config.js` modifier les valeurs des variables pour correspondre à votre architecture.